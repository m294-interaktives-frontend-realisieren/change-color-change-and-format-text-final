/* constants */
const elIdTextContainer = 'textContainer';

const classGolden = 'gold';
const classBlue = 'blue';
const origText = 'Originaltext';
const changedText = 'Geänderter Text';
const classItalic = 'italic';
const classBold = 'bold';

function changeText() {
  const elParaText = document.querySelector(`#${elIdTextContainer} p`);
  let textToChangeTo = origText;
  if (elParaText.textContent === origText) {
    textToChangeTo = changedText;
  }
  elParaText.textContent = textToChangeTo;
}

function getClasslistOfTextContainer() {
  const elTextContainer = document.getElementById(elIdTextContainer);
  return elTextContainer.classList;
}

function changeColor() {
  const classesTextContainer = getClasslistOfTextContainer();
  let classToRemove = classGolden;
  let classToAdd = classBlue;
  if (classesTextContainer.contains(classBlue)) {
    classToRemove = classBlue;
    classToAdd = classGolden;
  }
  classesTextContainer.remove(classToRemove);
  classesTextContainer.add(classToAdd);
}

function formatText() {
  const classesTextContainer = getClasslistOfTextContainer();
  let doAddClassItalic = true;
  if (classesTextContainer.contains(classItalic)) {
    doAddClassItalic = false;
    classesTextContainer.remove(classItalic);
    if (classesTextContainer.contains(classBold)) {
      classesTextContainer.remove(classBold);
    } else {
      classesTextContainer.add(classBold);
    }
  }
  if (doAddClassItalic) {
    classesTextContainer.add(classItalic);
  }
}

const elBtnChangeText = document.getElementById('change-text');
elBtnChangeText.addEventListener('click', changeText);

const elBtnChangeColor = document.getElementById('change-color');
elBtnChangeColor.addEventListener('click', changeColor);

const elBtnFormatText = document.getElementById('format-text');
elBtnFormatText.addEventListener('click', formatText);
